Here i will try to describe what each file will do and how it is organized.
Beside that, here we will have also information describing what we are doing each day in the projects within the project deadline

O branch master está dividido em duas pastas:
*  Testes
*  Medições

A pasta **Teste** é responsável em armazenar todos os testes para as arquiteturas utilizadas que não estejam prontas para a realização das medições, essa estando organizada da seguinte forma:
1. Arduino -> Pasta com alguns códigos realizados no arduino que possuem sensores que ainda não estão prontos para ir para os equipamentos que possuem antenas LoRa, vulgo MQ-x:
	* Arduino_MQ_DHT_SD - pasta com script para utilizacao no arduino que utiliza do sensor DHT e MQ e salva os dados em um cartao SD;
	* Arduino_MQ_DHT_SD_GPS - pasta com adicao do gps em relacao ao **Arduino_MQ_DHT_SD** com as funções separadas em arquivos separados mas que não funciona por causa do gps utilziado
	* Arduino_MQ_DHT_SD_GPS_v2 - segunda versao do mesmo script, a diferenca sendo que as funcoes estao presentes no mesmo arquivo do código principal
	* Calibragem_MQ9 - pasta com script e referências de como foi calculado as referências utilizadas para a calibragem do sensor MQ09;
	* Calibragem_MQ135 - similar a pasta descrita a cima só que voltado ao sensor MQ 135;
	* MQ9_referenceCalibration - Outro script de referencia para calibragem do MQ-09;
2. esp32Lora -> pasta referentes aos códigos destinados ao esp32 que utiliza de protocolo LoRaWan e podem se comunicar com o Dojot;
	1. esp32Dojot -> papsta com scripts para a comunicação MQTT entre o dispositvo e o Dojot
		* ESP32_DHT22_wifi_MQTT_Dojo - Envio de informações contínuas do sensor DHT por meio do Wi-fi utilizando o protocolo MQTT para a conexão do Dojot na ver 0.3.1
		* ESP32_wifi_MQTT_Dojo - Envio de um contador por meio do Wi-fi utilizando o protocolo MQTT para a conexão do Dojot na ver 0.3.1
	2. esp32Heltec -> pasta com arquivos específicos para o uso no esp32 Heltec que pode utilizar de suas funções internas já desenvolvidas;
		* esp32heltek_SD_DHT_MQ - script que tentou aproveitar do codigo ja funcional para o arduino, mas apresentou certos entraves quanto aos resultados mostrados, que não condiziam com o esperado.
		* Receiver - pasta com script referentes aos receptores de dados
			* DHT22_LoRa_Esp32_MQTT_dojot - Script que recebe dados de um sensor DHT por meio do protocolo LoRaWan e os envia para o DojoT por meio de MQTT; A mensagem, nesse código, é montada a partir de substrings.
			* LoRa_esp32_Serial - Script que recebe dados provenientes da comunicação LoRa e os escreve na entrada serial, ficando a cargo de um código javaScript o envio desses dados para o dojot por meio de protocolo MQTT
			* OLED_LoRa_DHT22_Serial - Script Similar ao **LoRa_esp32_Serial** só que printa as informações em uma tela OLED também.
		* Sender - pasta com script referentes aos emissores de dados
			* OLED_LoRa_DHT22 - Script para envio de dados do sensor DHT22 por meio do canal LoRa e utilizacao da tela de led para mostrar os dados coletados.
			* OLED_LoRa_DHT22_simple - Script similar a sua versão não simple, só que nesse os dados já são enviados de maneira mais simples de serem tratadas, com os dados separadas apenas por ':'.
		* Setup-Lora - Pasta com arquivos que tentaram realizar a coleta de dados MQ-x e DHT22 e o envio por meio de canal LoRa para outro dispositivo que ficaria responsável em separar esses dados e os enviar para um dispositivo virtual no DojoT, no entanto a coleta de dados MQ-X apresentou problemas;
		* Setup_Wi-Fi - Pasta com arquivos que tentaram realizar a coleta de dados MQ-x e DHT 22 realizando a comunicação do esp32 Heltec LoRa com o DojoT mas também apresentou problemas na coleta de dados do MQ-x
	3. esp32LoraGeneric -> pasta com arquivos genéricos para utilização de hardware que utilizam da comunicaçao LoRa. Especificando as portas utilizadas para cada finalidade, tendo como base o código disponível no github do **Lilygo ttgo tbeam**
		* OLED_LoRa_Receive - Script genérico de uso da comunicação LoRa com a utilização de um SSD de uso não obrigatório, código responsável em ser o receptor;
		* OLED_LoRa_Sender - Script genérico de uso da comunicação LoRa com a utilização de um SSD de uso não obrigatório, código responsável em ser o emissor;
3. JavaScript -> Pasta com scripts para comunicação com o dojot, tanto envio como recebimento de dados da plataforma do Dojot
	* Arquivos que fazem a leitura da porta serial e envia as informações para o dojot por meio do protocolo MQTT, esses scripts já estão atualizados para a sua utilização com a leitura de um baudrate específico, além de ser necessário informar alguns dados quanto ao device.

A pasta **Medições** é responsável em armazenar os setups que serão utilizados, ou já foram, para medições, estando organizados da seguinte forma:
1. Solicitaoes
	* endNode - Código que faz o envio de um contador junto com coordenadas de lat e long separadas por ' ', junto com a exibiciao das informacoes na tela para facilitar a visualizacao;
	* endNodeGPS - Código que faz o envio de um contador junto com coordenadas de lat e long, junto com a exibiciao das informacoes na tela para facilitar a visualizacao;
	* GatewayLoraSD - Códgio que recebe o que é enviado, mais especificamente informações de lat, long e contador, e as salva (realiza um append) dentro de um arquivo em um cartão SD
	* LoRa_SD_RSSI - Recebimento de um dado via LoRa, que representa o RSSI e sua escrita em um cartão SD.

2. Setup_Medicoes
	* Setup_Wifi ->
		* Setup_01_wifi - Pasta com Script responsável em fazer a coleta de dados do sensor e enviar via wi-fi para o Dojot por meio de protocolo MQTT, no entanto não teve muita sorte para distâncias além do teste de bancada, no entanto é um código funcional.
		* Setup_02_LoRa - Pasta com Script responsável em fazer a troca de dados entre dispositivos LoRa, com um enviando informações referentes ao sensores que está medindo e o outro responsável em enviar as informações via Wi-Fi e por meio de protocolo MQTT para o dojoT junto com métricas referente ao canal Wi-Fi e LoRa (RSSI e SNR), com o back-up dessas informações sendo feitas em um cartão SD, aprensenta o mesmo problema do setup_01 mas continua sendo um código funcional;
	* Setup_Serial ->
		* Setup_01_Serial - **Setup com medicoes marcadas para a semana de 13 de janeiro.** Esse Setup realiza a comunicação entre LoRa e envia via Serial para o Dojot. O Emissor possui um sensor DHT22 e um cartão SD, sendo esse o responsável em fazer a coleta do sensor, salvar as informações medidas no cartão SD e as enviar via LoRa para o dispositivo Receptor, que atuará como Gateway. Esse outro dispositivo fica responsável em receber esses dados, separá-los, adicionar informações referentes ao canal e exibi-las na porta serial para que um script Js seja a responsável em enviar as informações para o dojot.
		* Setup_01_Generico - Setup para medicoes utilizando comunicacao entre ttgo e loraheltec para utilizacao tanto do sensor DHT, do GPS e do SD.
		* Setup_02_Serial_Salto - Setup para comunicação LoRa além do alcance padrão, com a utilização de um dispositivo intermediário que vai ficar na espera da chegada de um pacote para o repassar a um outro device

Com o sistema de arquivos já descritos, o espaço restante é responsável em registrar as atividades realizadas em cada dia durante o período do projeto, sendo separado por integrante do grupo:
1. Flaviane Louzeiro
*  13/01 
    * Manutenção do Desktop onde o Dojot está instalado;
    * Montagem do cronograma;
    * Reunião para a definição da configuração dos setups de medição;
    * Estruturação das atividades diária da equipe local;

*  14/01 
    * Pseudo-código dos setups;
    * Revisão e ajuste nos códigos entregues no dia.

* 15/01
    * Teste da integração das plataforma TTGO e os Esp32;

* 20/01
    * Continuação dos trabalhos do dia 15/01.
    * Estudo da API de consulta do Dojot;

* 21/01
    * Elaboração do scritp de Consulta e Tratamento dos dados;
    * Medição do Setup 01 - Dojot--Serial--Lora====Lora--Sensor.

* 22/01


2. Felipe Reis
*  13/01
    * Organização dos scripts presentes no gitlab;
    * Montagem do cronograma virtual no gitlab para acompanhamento;
	* Teste de funcionamento do Data logging board. Obs: Protoboard com mal contato.
*  14/01
    * Realizacao de um Script generico para a utilizacao do TTGO
    * Script para comunicacao em saltos para o segundo setup  
*  15/01
    * Continuacao das ativadades do dia 14/01 com a definicao dos dispositivos a serem utilizados no setup02, após a realização de alguns testes com sensores.
*  20/01
    * Inicio de trabalho com o dragino e entendimento de como upar os códigos para o gateway e os nodes
    * Finalizacao do cronograma de acompanhamento Físico
    * Testes dos códigos de setup para medicoes no dia 21/01
* 21/01
	* Continuação das revisões dos códigos para as medições do setup de saltos com serial, faltando apenas otimizar o gateway com o recebimento das informações
	* Realização da primeira medição, de 10 em 10 metros, de 0 a 130m, em visada direta, na altura de 1.20 a 1.30 metros, coletados 100 pontos a cada pontos com o envio dos dados para o Dojot, com a observação de que não foram enviados os dados de RSSI e SNR por erro de nomenclatura na hora de atributo na hora do envio, além de ser necessário ordenar os dados salvos no cartão SD disponíveis no gitlab.
* 22/01
	* Ilustração das arquiteturas utilizadas e futuras, junto com um mapa de medição simulando os pontos que foram e serão medidos. link: 
	https://docs.google.com/presentation/d/1_CjGV-_yWB3Ku6P9KAejg5R4KmHXeKMd3zoSdCs13Wo/edit#slide=id.g6df0c66719_0_24
	* Modificação do código do gateway genérico para sua melhor operação.
* 23/01
	* Realização das medições de visada direta utilizando o LoRa de forma correta, teste de NLoS com LoRa para saber até onde ele vai (chegamos até lab de mecânica e tivemos ainda sinal com RSSI médio -120);
	* Adição dos dados méédidos no slide das arquiteturas e medições

3. Kaique da Silva
*  13/01
    * Inicío do trabalho com o Flowchart
* 14/01 
*   *Teste de instalação do Dojot nos sistemas Debian e ubuntu server
   
*  18/01
    *Estudo de envio de dados de dispositivos fisicos para dispositivos virtuais e execução de flows.

*  30/01
    *Inicio dos estudos com Kubernets e Ubuntu server 18.4.


