/*
  Enviando informacoes para o dojot por meio de e lora com backup no cartao SD.
  Metricas que sao levadas em consideracao: RSSI Lora, latencia, jitter, packetloss.
  Nesse teste foram ultizados um Data logging board,esp32helteklora.
  Circuito
   SD card attached to SPI bus as follows:
 **SD_CS   -- GPIO22
 **SD_MOSI -- GPIO23
 **SD_SCK  -- GPIO17
 **SD_MISO -- GPIO13
  created   Nov 2019
  by LCTTeam
*/

#include <SPI.h>
#include <SD.h>
#include "FS.h"
#include "heltec.h"

SPIClass spi1;

#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

unsigned int counter = 0;
String rssi = "RSSI --";
String packSize = "--";
String packet;

int cont = 0;

unsigned long startTime = 0;
unsigned long endTime = 0;
float snrLora = 0;
long rssiLora = 0;

int packLoss = 0;

void setup() {
  Serial.begin(9600);
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, false /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Heltec.display->init();
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");

  Heltec.display->drawString(0, 20, "initialization done.");
  Heltec.display->display();
  delay(2000);
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(500);
  String contS = " ";
  String latS = " ";
  String lonS = " ";

  int packetSize = LoRa.parsePacket();

  String lorapacket;
  if (packetSize) {
    // read packet
    while (LoRa.available()) {
      lorapacket += String((char)LoRa.read());
    }
    rssiLora = LoRa.packetRssi();
    snrLora = LoRa.packetSnr();

    contS = getValue(lorapacket, ' ', 0);
    latS = getValue(lorapacket, ' ', 1);
    lonS = getValue(lorapacket, ' ', 2);

    //String payload1 = "{\"temp\":\"" + tempS + "\"}";
    //String payload2 = "{\"umid\":\"" + umiS + "\"}";
    Serial.println(lorapacket);
    String payload = lorapacket + " " + String(rssiLora) + " " + String(snrLora); 
    appendFile(SD, "/teste.txt", payload);
  }
}

void appendFile(fs::FS & fs, const char * path, String message) {
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if (!file) {
      Serial.println("Failed to open file for appending");
      return;
    }
    if (file.print(message)) {
      Serial.println("Message appended");
    } else {
      Serial.println("Append failed");
    }
    file.close();
  }
