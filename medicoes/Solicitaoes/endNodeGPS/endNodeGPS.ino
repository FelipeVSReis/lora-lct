#include <SPI.h>
#include <SD.h>
#include "FS.h"
#include <LoRa.h>
#include <Wire.h>  
#include <TinyGPS++.h>
#include "SSD1306.h" 

HardwareSerial Seriali(1);

#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISnO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND  915E6

float lat,lon;
unsigned int counter = 0;
static const int RXPin = 3, TXPin = 1; //rx(GPI03),tx(GPI01)
static const uint32_t GPSBaud = 4800;


SSD1306 display(0x3c, 4, 15); //SDA - SCL
TinyGPSPlus gps; // create gps object 
String rssi = "RSSI --";
String packSize = "--";
String packet ;
 

void setup() {
  pinMode(16,OUTPUT);
  pinMode(2,OUTPUT);
  
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50); 
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high

  Seriali.begin(25000, SERIAL_8N1, 3,1);
  Serial.begin(115200);
  while (!Serial);
  Serial.println();
  Serial.println("LoRa Sender Test");
  
  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  //LoRa.onReceive(cbk);
  //LoRa.receive();
  Serial.println("init ok");
  display.init();
  display.flipScreenVertically();  
  display.setFont(ArialMT_Plain_10);
   
  delay(1500);
}

void loop() {
  display.clear();
  smartDelay(1000);

  if (millis() > 5000 && gps.charsProcessed() < 10)
    Serial.println(F("No GPS data received: check wiring"));

  lat = gps.location.lat();
  lon = gps.location.lng();
  String latS = String(lat);
  String lonS = String(lon);
  String payload = "lat: " + latS + " long: " + lonS;
  
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  
  display.drawString(0, 0, "Sending packet: ");
  display.drawString(90, 0, String(counter));
  display.drawString(0, 20, "lat:");
  display.drawString(80, 20, latS);
  display.drawString(0, 30, "lon:");
  display.drawString(80, 30, lonS);
  Serial.println(String(counter));
  display.display();

  // send packet
  LoRa.beginPacket();
  LoRa.print("hello ");
  LoRa.print(counter);
  LoRa.print("...");
  LoRa.print("lat: ");
  LoRa.print(lat);
  LoRa.print(" lon: ");
  LoRa.print(lon);
  LoRa.endPacket();
  
  counter++;
  digitalWrite(2, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(2, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (Seriali.available())
      gps.encode(Seriali.read());
  } while (millis() - start < ms);
}
