void reconectaBroker()
  {
    //Conexao ao broker MQTT
    client.setServer(mqttServer, mqttPort);
    while (!client.connected())
    {
      Heltec.display->clear();
      Heltec.display->drawString(0, 0, "Conectando ao broker MQTT");
      //Serial.println("Conectando ao broker MQTT...");
      if (client.connect("ESP32Client", mqttUser, mqttPassword ))
      {
        Heltec.display->drawString(0, 10, "Conectando ao broker!");
        //Serial.println("Conectado ao broker!");
      }
      else
      {
        Heltec.display->drawString(0, 10, "Falha na conexao ao broker");
        Heltec.display->drawString(0, 20, "Estado:");
        Heltec.display->drawString(40, 20, String(client.state()));
        //Serial.print("Falha na conexao ao broker - Estado: ");
        //Serial.print(client.state());
        Heltec.display->display();
        delay(2000);
      }
    }
  }
