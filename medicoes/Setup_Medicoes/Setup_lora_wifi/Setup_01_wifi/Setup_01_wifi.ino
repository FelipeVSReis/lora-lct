/*
  Enviando informacoes para o dojo por meio de wifi com backup no cartao SD.
  Metricas que sao levadas em consideracao: RSSI, latencia, jitter, packetloss.
  Nesse teste foram ultizados um Data logging board,dht22,esp32helteklora.
  Circuito
   SD card attached to SPI bus as follows:
 **SD_CS   -- GPIO22
 **SD_MOSI -- GPIO23
 **SD_SCK  -- GPIO17
 **SD_MISO -- GPIO13
   DHT22
 ** Digital - pin 2
  created   Nov 2019
  by LCTTeam
*/
#include <SPI.h>
#include <SD.h>
#include <Adafruit_Sensor.h>
#include "DHT.h"
#include "FS.h"
#include <time.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include "heltec.h"
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

SPIClass spi1;

#define DHTPIN 2
#define DHTTYPE DHT22

const char* ssid     = "LCT";
const char* password = "propagacao";
const char* mqttServer = "192.168.0.116";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "admin";
int cont = 0;
int packLoss = 0;

long timezone = 1;
byte daysavetime = 1;
unsigned long startTime= 0;
unsigned long endTime= 0;
long rssi = 0;
DHT dht(DHTPIN, DHTTYPE);
WiFiClient espClient;
PubSubClient client(mqttServer, mqttPort, NULL, espClient);

char mqttTopic[] = "/admin/dee7f6/attrs";


void setup() {
  Serial.begin(9600);
  dht.begin();
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);
  WiFi.begin(ssid, password);
  Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Disable*/, false/*Serial Enable*/);
  Heltec.display->init();
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_10);

  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Heltec.display->drawString(0, 10, "Iniciando conexao WiFi..");
  }
  Heltec.display->drawString(0, 20, "Conectado na rede " + String(ssid));
  if (!SD.begin(22, spi1)) {
    Heltec.display->drawString(0, 30, "Card Mount Failed");
    return;
  }

  uint8_t cardType = SD.cardType();

  configTime(3600 * timezone, daysavetime * 3600, "time.nist.gov", "0.pool.ntp.org", "1.pool.ntp.org");
  struct tm tmstruct ;
  delay(2000);
  tmstruct.tm_year = 0;
  getLocalTime(&tmstruct, 5000);
  String timeNow = String((tmstruct.tm_year) + 1900)+String("/")+String(( tmstruct.tm_mon)+ 1)+String("/")+String(tmstruct.tm_mday)+String(" ")+String(tmstruct.tm_hour-4)+String(":")+String(tmstruct.tm_min)+String(":")+String(tmstruct.tm_sec)+"\n";
  appendFile(SD, "/setup.txt", timeNow);

  Heltec.display->drawString(0, 40, "initialization done.");
  Heltec.display->drawString(0, 50, String(WiFi.localIP()));
  Heltec.display->display();
  delay(1000);
}

void loop() {
  struct tm tmstruct;
  delay(1000);
  tmstruct.tm_year = 0;
  getLocalTime(&tmstruct, 5000);
  Heltec.display->clear();
  delay(500);
  reconectaBroker();
  
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t))
  {
    Heltec.display->drawString(0, 10, "Falha na leitura do sensor");
    return;
  }
  if (cont <= 100) {
    
    String umiS = String(h);
    String tempS = String(t);
    String payload1 = "{\"temp\":\"" + tempS + "\"}"; //conforme está cadastrado no dojot
    String payload2 = "{\"umid\":\"" + umiS + "\"}"; //conforme está cadastrado no dojot

    String timeNow = String(tmstruct.tm_mday)+String("/")+String(( tmstruct.tm_mon)+ 1)+String("/")+String((tmstruct.tm_year) + 1900)+String(" ")+String(tmstruct.tm_hour-4)+String(":")+String(tmstruct.tm_min)+String(":")+String(tmstruct.tm_sec)+"\n";
    startTime = micros();
    if (client.publish(mqttTopic, (char*) payload1.c_str())) {
      endTime = micros ();
      Heltec.display->drawString(0, 0, "publish payload 1");
    } else {
      Heltec.display->drawString(0, 0, "publish 1 failed");
      Serial.print("publish 1 failed");
      packLoss +=1; //armazenar a quant de pacotes perdidos
    }
    appendFile(SD, "/setup.txt", payload1);
    appendFile(SD, "/setup.txt", ",latencia: " + String(endTime - startTime) + ",");
    rssi = WiFi.RSSI();
    appendFile(SD, "/setup.txt","RSSI: " + String(rssi) + ',');
    appendFile(SD, "/setup.txt", timeNow);
    

    Heltec.display->drawString(0, 10, "payload 1 salvo no SD");
    
    timeNow = String(tmstruct.tm_mday)+String("/")+String(( tmstruct.tm_mon)+ 1)+String("/")+String((tmstruct.tm_year) + 1900)+String(",")+String(tmstruct.tm_hour-4)+String(":")+String(tmstruct.tm_min)+String(":")+String(tmstruct.tm_sec)+"\n";
    startTime = micros();
    if (client.publish(mqttTopic, (char*) payload2.c_str())) {
      endTime = micros();
      Heltec.display->drawString(0, 20, "publish payload 2");
    } else {
      Heltec.display->drawString(0, 20, "publish 2 failed");
      Serial.print("publish 2 failed");
      packLoss +=1; //armazenar a quant de pacotes perdidos
    }
    appendFile(SD, "/setup.txt", payload2);
    appendFile(SD, "/setup.txt", ",latencia: " + String(endTime - startTime) + ",");
    rssi = WiFi.RSSI();
    appendFile(SD, "/setup.txt","RSSI: " + String(rssi) + ',');
    appendFile(SD, "/setup.txt", timeNow);
    
    Heltec.display->drawString(0, 30, "payload 2 salvo no SD");
    Heltec.display->drawString(0, 40, String(cont)); //lembrar que sao 100*2 pacotes
    Heltec.display->display();
    cont += 1;
  } else {
    Heltec.display->clear();
    Heltec.display->drawString(0, 50, "100 iteracoes feitas");
    Heltec.display->display();
    getLocalTime(&tmstruct, 5000);
    String timeEnd = String((tmstruct.tm_year) + 1900) +String("/")+ String (( tmstruct.tm_mon) + 1) +String("/")+ String(tmstruct.tm_mday) +String(",")+ String(tmstruct.tm_hour-4)+ String(":") + String(tmstruct.tm_min) +String(":")+ String(tmstruct.tm_sec+ "\n");
    delay (2000);
    appendFile(SD, "/setup.txt", "Pacotes perdidos: " +String(packLoss)+'\n'); //salvando a qtd perdida
    appendFile(SD, "/setup.txt", timeEnd);
    cont=0;
    delay (10000); //para andar para o prox ponto a medir
    
  }
}
