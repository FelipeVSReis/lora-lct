/*
  Enviando informacoes para o dojot por meio de wifi e lora com backup no cartao SD.
  Metricas que sao levadas em consideracao: RSSI Lora e Wifi, latencia, jitter, packetloss.
  Nesse teste foram ultizados um Data logging board,esp32helteklora.
  Circuito
   SD card attached to SPI bus as follows:
 **SD_CS   -- GPIO22
 **SD_MOSI -- GPIO23
 **SD_SCK  -- GPIO17
 **SD_MISO -- GPIO13
  created   Nov 2019
  by LCTTeam
*/
#include <SPI.h>
#include <SD.h>
#include "FS.h"
#include <time.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include "heltec.h"

SPIClass spi1;

#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

unsigned int counter = 0;
String rssi = "RSSI --";
String packSize = "--";
String packet;

const char* ssid     = "LCT";
const char* password = "propagacao";
const char* mqttServer = "192.168.0.116";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "admin";
int cont = 0;
int packLoss = 0;

long timezone = 1;
byte daysavetime = 1;
unsigned long startTime = 0;
unsigned long endTime = 0;
float snrLora = 0;
long rssiLora = 0;
long rssiWiFi = 0;

WiFiClient espClient;
PubSubClient client(mqttServer, mqttPort, NULL, espClient);

char mqttTopic[] = "/admin/5fe2d0/attrs";

void setup() {
  Serial.begin(9600);
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);
  WiFi.begin(ssid, password);
  //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Heltec.display->init();
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Heltec.display->drawString(0, 10, "Iniciando conexao WiFi..");
  }
  Heltec.display->drawString(0, 20, "Conectado na rede " + String(ssid));

  if (!SD.begin(22, spi1)) {
    Heltec.display->drawString(0, 30, "Card Mount Failed");
    return;
  }

  uint8_t cardType = SD.cardType();

  configTime(3600 * timezone, daysavetime * 3600, "time.nist.gov", "0.pool.ntp.org", "1.pool.ntp.org");
  struct tm tmstruct ;
  delay(2000);
  tmstruct.tm_year = 0;
  getLocalTime(&tmstruct, 5000);
  String timeNow = String((tmstruct.tm_year) + 1900) + String("/") + String(( tmstruct.tm_mon) + 1) + String("/") + String(tmstruct.tm_mday) + String(" ") + String(tmstruct.tm_hour - 4) + String(":") + String(tmstruct.tm_min) + String(":") + String(tmstruct.tm_sec) + "\n";
  appendFile(SD, "/setup.txt", timeNow);

  Heltec.display->drawString(0, 40, "initialization done.");
  Heltec.display->display();
  delay(2000);
}

void appendFile(fs::FS &fs, const char * path, String message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void reconectaBroker()
{
  //Conexao ao broker MQTT
  client.setServer(mqttServer, mqttPort);
  while (!client.connected())
  {
    Heltec.display->clear();
    Heltec.display->drawString(0, 0, "Conectando ao broker MQTT");
    //Serial.println("Conectando ao broker MQTT...");
    if (client.connect("ESP32Client", mqttUser, mqttPassword ))
    {
      Heltec.display->drawString(0, 10, "Conectando ao broker!");
      //Serial.println("Conectado ao broker!");
    }
    else
    {
      Heltec.display->drawString(0, 10, "Falha na conexao ao broker");
      Heltec.display->drawString(0, 20, "Estado:");
      Heltec.display->drawString(40, 20, String(client.state()));
      //Serial.print("Falha na conexao ao broker - Estado: ");
      //Serial.print(client.state());
      Heltec.display->display();
      delay(2000);
    }
  }
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void loop() {
  struct tm tmstruct;
  delay(2000);
  tmstruct.tm_year = 0;
  getLocalTime(&tmstruct, 5000);
  Heltec.display->clear();
  delay(500);
  String contS = " ";
  String umiS = " ";
  String tempS = " ";
  reconectaBroker();

  int packetSize = LoRa.parsePacket();
  String lorapacket;
  if (packetSize) {
    // read packet
    while (LoRa.available()) {
      lorapacket += String((char)LoRa.read());
    }
    rssiLora = LoRa.packetRssi();
    snrLora = LoRa.packetSnr();

    String contS = getValue(lorapacket, ':', 0);
    String tempS = getValue(lorapacket, ':', 1);
    String umiS = getValue(lorapacket, ':', 2);

    String payload1 = "{\"temp\":\"" + tempS + "\"}";
    String payload2 = "{\"umid\":\"" + umiS + "\"}";

    String timeNow = String(tmstruct.tm_mday) + String("/") + String(( tmstruct.tm_mon) + 1) + String("/") + String((tmstruct.tm_year) + 1900) + String(" ") + String(tmstruct.tm_hour - 4) + String(":") + String(tmstruct.tm_min) + String(":") + String(tmstruct.tm_sec) + "\n";
    startTime = micros();
    if (client.publish(mqttTopic, (char*) payload1.c_str())) {
      endTime = micros ();
      Heltec.display->drawString(0, 0, "publish payload 1");
    } else {
      Heltec.display->drawString(0, 0, "publish 1 failed");
      packLoss += 1; //armazenar a quant de pacotes perdidos
    }
    appendFile(SD, "/setup02.txt", contS + ", ");
    appendFile(SD, "/setup02.txt", payload1);
    appendFile(SD, "/setup02.txt", ",latencia: " + String(endTime - startTime) + ",");
    rssiWiFi = WiFi.RSSI();
    appendFile(SD, "/setup02.txt", "RSSIWifi: " + String(rssiWiFi) + ",");
    appendFile(SD, "/setup02.txt", timeNow);

    Heltec.display->drawString(0, 10, "payload 1 salvo no SD");

    timeNow = String(tmstruct.tm_mday) + String("/") + String(( tmstruct.tm_mon) + 1) + String("/") + String((tmstruct.tm_year) + 1900) + String(",") + String(tmstruct.tm_hour - 4) + String(":") + String(tmstruct.tm_min) + String(":") + String(tmstruct.tm_sec) + "\n";
    startTime = micros();
    if (client.publish(mqttTopic, (char*) payload2.c_str())) {
      endTime = micros();
      Heltec.display->drawString(0, 20, "publish payload 2");
    } else {
      Heltec.display->drawString(0, 20, "publish 2 failed");
      packLoss += 1; //armazenar a quant de pacotes perdidos
    }
    cont = contS.toInt();
    contS = String(cont+1);
    appendFile(SD, "/setup02.txt", contS + ", ");
    appendFile(SD, "/setup02.txt", payload2);
    appendFile(SD, "/setup02.txt", ",latencia: " + String(endTime - startTime) + ",");
    rssiWiFi = WiFi.RSSI();
    appendFile(SD, "/setup02.txt", "RSSIWifi: " + String(rssiWiFi) + ',');
    appendFile(SD, "/setup02.txt", "RSSILoRa: " + String(rssiLora) + ',');
    appendFile(SD, "/setup02.txt", "SnrLora: " + String(snrLora) + ',');
    appendFile(SD, "/setup02.txt", timeNow);

    Heltec.display->drawString(0, 30, "payload 2 salvo no SD");
    Heltec.display->display();
    if (cont == 100) {
      Heltec.display->clear();
      Heltec.display->drawString(0, 50, "100 iteracoes feitas");
      Heltec.display->display();
      getLocalTime(&tmstruct, 5000);
      String timeEnd = String((tmstruct.tm_year) + 1900) + String("/") + String (( tmstruct.tm_mon) + 1) + String("/") + String(tmstruct.tm_mday) + String(",") + String(tmstruct.tm_hour - 4) + String(":") + String(tmstruct.tm_min) + String(":") + String(tmstruct.tm_sec + "\n");
      delay (2000);
      appendFile(SD, "/setup02.txt", "Pacotes perdidos: " + String(packLoss) + '\n'); //salvando a qtd perdida
      appendFile(SD, "/setup02.txt", timeEnd);
      cont = 0;
      delay(3000);
    }
  }
}
