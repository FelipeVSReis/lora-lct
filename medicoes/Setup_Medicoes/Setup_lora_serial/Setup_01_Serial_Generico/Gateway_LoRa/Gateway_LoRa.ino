/*
  Enviando informacoes para o dojot por meio de Serial e recebendo pacote por meio do lora.
  Metricas que sao levadas em consideracao: RSSI Lora, SNR Lora.
  Nesse teste foram ultizados apenas um esp32helteklora.
  created   Nov 2019
  by LCTTeam
*/

#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>
#include "SSD1306.h"

#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    915E6

//SPIClass spi1;

SSD1306 display(0x3c, 4, 15);
String rssi2 = "--";
String snr2 = "--";
String packSize = "--";
String packet;

int cont = 0;

unsigned long startTime = 0;
unsigned long endTime = 0;

int packLoss = 0;

void setup() {
  
  Serial.begin(115200);
  while (!Serial);
  Serial.println();
  Serial.println("LoRa Gateway");
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  //LoRa.onReceive(cbk);
  LoRa.receive();
  Serial.println("init ok");
  delay(1500);
}

/*
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
*/

void loop() {
  // put your main code here, to run repeatedly:
  rssi2="";
  snr2="";
  String packet = "";

  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // read packet
    packet = "";
    packSize = String(packetSize, DEC);
    for (int i = 0; i < packetSize; i++) {
     packet += (char) LoRa.read();
     }
      rssi2 = String(LoRa.packetRssi());
      snr2 = String(LoRa.packetSnr());
      packet += ":"+rssi2 + ":" + snr2;
      Serial.println(packet);
    }

    //String contS = getValue(lorapacket, ':', 0);
    //String tempS = getValue(lorapacket, ':', 1);
    //String umiS = getValue(lorapacket, ':', 2);
    //String payload1 = "{\"temp\":\"" + tempS + "\"}";
    //String payload2 = "{\"umid\":\"" + umiS + "\"}";
  }
