const SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;
const port = new SerialPort('/dev/ttyUSB0', {baudRate: 115200});
const parser = new Readline();
port.pipe(parser);
 
 
var mqtt = require('mqtt')
//var client  = mqtt.connect('mqtt://192.168.42.28')
//var client  = mqtt.connect('mqtt://192.168.0.103')
var client  = mqtt.connect('mqtt://192.168.0.116')
//var client  = mqtt.connect('mqtt://localhost:8000')
 
console.log('inicio');
client.on('connect', function () {
console.log('conectou');    
client.subscribe('/admin/3955c8/attrs');
 
parser.on('data', (recebido_arduino) => {
  console.log("==========================");    
  console.log("recebe: " + recebido_arduino);
  var leitura_gateway = recebido_arduino.split(':');
  console.log("Contador: " + leitura_gateway[0]);
  console.log("Temperatura: " + leitura_gateway[1]);
  console.log("Umidade: " + leitura_gateway[2]);
  console.log("RSSILora: " + leitura_gateway[3]);  
  console.log("SNRLora: " + leitura_gateway[4]);  
  client.publish("/admin/3955c8/attrs", '{"cont":'+ leitura_gateway[0] +'}');
  client.publish("/admin/3955c8/attrs", '{"temp":'+ leitura_gateway[1] +'}');
  client.publish("/admin/3955c8/attrs", '{"umid":'+ leitura_gateway[2] +'}');
  client.publish("/admin/3955c8/attrs", '{"SNR_1":'+ leitura_gateway[3] +'}');
  client.publish("/admin/3955c8/attrs", '{"RSSi_1":'+ leitura_gateway[4] +'}');
  client.publish("/admin/3955c8/attrs", '{"SNR_2":'+ leitura_gateway[5] +'}');
  client.publish("/admin/3955c8/attrs", '{"RSSI_2":'+ leitura_gateway[6] +'}');
})  
  
})
client.on('message', function (topic, message) {  
  console.log("msg recebida => " + message.toString());  
})
