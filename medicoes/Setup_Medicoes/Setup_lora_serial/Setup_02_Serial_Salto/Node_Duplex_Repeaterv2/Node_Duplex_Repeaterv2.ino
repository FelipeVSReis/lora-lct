/*
  Código Genérico para o repasse de pacotes por meio de LoRa, onde o pacote é escutado e repassado para o próximo dispositivo
  com a adição de SNR e RSSI do canal LoRa
*/

#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>
#include "SSD1306.h"
#include "images.h"

#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    915E6

SSD1306 display(0x3c, 4, 15);
String rssi1 = "--";
String snr1 = "--";
String packSize = "--";
String packet;
String packetSend;
String lastSendCounterS;        // last send counter string
int lastSendCounter = 1;

void loraData() {
  //display.clear();
  //display.setTextAlignment(TEXT_ALIGN_LEFT);
  //display.setFont(ArialMT_Plain_10);
  //display.drawString(0 , 15 , "Received "+ packSize + " bytes");
  //display.drawStringMaxWidth(0 , 26 , 128, packet);
  //display.drawString(0, 0, rssi);
  //display.display();
  Serial.print("RSSI1:" + rssi1);
  Serial.print("SNR1:" + snr1);
  Serial.println(packet);
}

void cbk(int packetSize) {
  packet = "";
  packSize = String(packetSize, DEC);
  for (int i = 0; i < packetSize; i++) {
    packet += (char) LoRa.read();
  }
  rssi1 = String(LoRa.packetRssi(), DEC);
  snr1 = String(LoRa.packetSnr());
  loraData();
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void setup() {
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50);
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high、

  Serial.begin(115200);
  while (!Serial);
  Serial.println();
  Serial.println("LoRa Receiver Callback");
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  //LoRa.onReceive(cbk);
  LoRa.receive();
  Serial.println("init ok");
  //display.init();
  //display.flipScreenVertically();
  //display.setFont(ArialMT_Plain_10);

  delay(1500);
}

void loop() {
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    cbk(packetSize);
  }
  String control = getValue(packet,':',0);
  int currentCounter = control.toInt();
  if (currentCounter!= lastSendCounter) {
    packetSend = packet + ":" + rssi1 + ":" + snr1;
    sendMessage(packetSend);
    lastSendCounterS = getValue(packetSend,':',0);
    lastSendCounter = lastSendCounterS.toInt();
  }
}

void sendMessage(String outgoing) {

  LoRa.beginPacket();                   // start packet
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  Serial.println(outgoing);
}
