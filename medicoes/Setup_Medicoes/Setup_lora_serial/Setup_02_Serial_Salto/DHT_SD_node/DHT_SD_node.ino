/*
 * Nesse teste foram ultizados um sensor DHT, um esp32helteklora  e um cartao SD.
  OLED_SDA -- GPIO4
  OLED_SCL -- GPIO15
  OLED_RST -- GPIO16
  Circuito
  DHT22
 **Digital - Pin 2
  Data loggin board
 * SCK  --  GPIO17
 * CS   --  GPIO22
 * MOSI --  GPIO23
 * MISO --  GPIO13
  created Nov 2019
  by LCTTeam
*/

#include <SPI.h>
#include <SD.h>
#include "FS.h"
#include <Adafruit_Sensor.h>
#include "DHT.h"
#include "SSD1306.h"
#include <LoRa.h>
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

#define DHTPIN 2
#define DHTTYPE DHT11
#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    915E6

SSD1306 display(0x3c, 4, 15);
String packSize = "--";
String packet;
unsigned int cont = 0;

DHT dht(DHTPIN, DHTTYPE);
SPIClass spi1;

void setup() {
  //WIFI Kit series V1 not support Vext control
  Serial.begin(115200);
  Serial.println("comecou");
  
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);
  dht.begin();
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    display.drawString(0, 20, "Starting LoRa Failed");
    return;
  }
  LoRa.setSpreadingFactor(7); 
  if (!SD.begin(22, spi1)) {
    Serial.println("Card Mount failed!");
    display.drawString(0, 30, "Card Mount Failed");
    return;
  }
  display.drawString(0, 40, "initialization done.");
  Serial.println("init done");
  display.display();
  delay(1500);
}

void appendFile(fs::FS &fs, const char * path, String message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(500);
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t))
  {
    display.drawString(0, 10, "Falha na leitura do sensor");
    Serial.println("leitura errada");
    display.display();
    return;
  }
  if (cont <= 100) {
    display.drawString(0, 0, "Sending packet: ");
    display.drawString(90, 0, String(cont));

    String tempS = String(t);
    String umiS = String(h);
    String payload = String(cont) + ":" + tempS + ":" + umiS;
    display.drawString(0, 10, "temp:");
    display.drawString(40, 10, tempS);
    display.drawString(0, 20, "umid:");
    display.drawString(40, 20, umiS);
    display.display();
    
    LoRa.beginPacket();
    LoRa.print(payload);
    LoRa.endPacket();
    cont += 1;
    Serial.println(payload);
    appendFile(SD, "/setupEmerg.txt", payload);
  }else{
    display.clear();
    display.drawString(0, 50, "100 iteracoes feitas");
    display.display();
    Serial.println("100 iteracoes feitas");
    }
  
}
