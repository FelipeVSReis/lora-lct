/*
  Comunicacao LoRa Duplex
  Aguarda receber algo, assim que receber algo envia o pacote recebido.
  LoRa Heltec esp32 v2

*/

#include <SPI.h>              // include libraries
#include <LoRa.h>
#include <Wire.h>
#include "SSD1306.h" 

#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    915E6

String outgoing;              // outgoing message

long lastSendTime = 0;        // last send time
//int interval = 2000;         // interval between sends
int interval = 200;          // interval between sends
float snrLora = 0;
long rssiLora = 0;
SSD1306 display(0x3c, 4, 15);

void setup() {
  pinMode(16,OUTPUT);
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50);
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high、
  
  Serial.begin(115200);
  while (!Serial);
  Serial.println("LoRa Duplex");
  SPI.begin(SCK, MISO, MOSI, SS);
  // override the default CS, reset, and IRQ pins (optional)
  LoRa.setPins(SS, RST, DI0); // set CS, reset, IRQ pin
  if (!LoRa.begin(915E6)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }
  LoRa.receive();
  Serial.println("LoRa init succeeded.");

  delay(1500);
}

void loop() {
  // read packet;
  String incoming = "";
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    String packSize = String(packetSize, DEC);
    for (int i = 0; i < packetSize; i++) {
      incoming += (char) LoRa.read();
      rssiLora = LoRa.packetRssi();
    }
  }
  Serial.println(packetSize + ":" + rssiLora);
}
/*snrLora = LoRa.packetSnr();
  rssiLora = LoRa.packetRssi();
  incoming = incoming + " RSSI1: " + String(rssiLora) + " snrLora1: " + String(snrLora);
  Serial.println(incoming);
  if(millis() - lastSendTime > interval){
  sendMessage(incoming);
  lastSendTime = millis();            // timestamp the message
  //    interval = random(2000) + 1000;    // 2-3 seconds
  interval = random(200) + 100;    // 0.2-0.3 seconds
  }*/


void sendMessage(String outgoing) {
  LoRa.beginPacket();                   // start packet
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
}
