/*
 * Nesse teste foram ultizados um sensor DHT e um esp32helteklora.
  OLED_SDA -- GPIO4
  OLED_SCL -- GPIO15
  OLED_RST -- GPIO16
  Circuito
  DHT22
 **Digital - Pin 2
  created Nov 2019
  by LCTTeam
*/

#include <SPI.h>
#include <SD.h>
#include "FS.h"
#include <Adafruit_Sensor.h>
#include "DHT.h"
#include "heltec.h"
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

#define DHTPIN 2
#define DHTTYPE DHT11
//#define DHTTYPE DHT22
unsigned int cont = 0;

DHT dht(DHTPIN, DHTTYPE);
SPIClass spi1;

void setup() {
  //WIFI Kit series V1 not support Vext control
  //Serial.begin(9600);
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);
  dht.begin();
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Helte.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Heltec.display->init();
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_10);
  if (!SD.begin(22, spi1)) {
    Heltec.display->drawString(0, 30, "Card Mount Failed");
    return;
  }
  Heltec.display->drawString(0, 40, "initialization done.");
  Heltec.display->display();
  delay(2000);
}

void appendFile(fs::FS &fs, const char * path, String message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}


void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_10);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t))
  {
    Heltec.display->drawString(0, 10, "Falha na leitura do sensor");
    return;
  }
  if (cont <= 100) {
    Heltec.display->drawString(0, 0, "Sending packet: ");
    Heltec.display->drawString(90, 0, String(cont));

    String tempS = String(t);
    String umiS = String(h);
    String payload = String(cont) + ":" + tempS + ":" + umiS;
    Heltec.display->drawString(0, 10, "temp:");
    Heltec.display->drawString(40, 10, tempS);
    Heltec.display->drawString(0, 20, "umid:");
    Heltec.display->drawString(40, 20, umiS);
    Heltec.display->display();
    /*
      LoRa.setTxPower(txPower,RFOUT_pin);
      txPower -- 0 ~ 20
      RFOUT_pin could be RF_PACONFIG_PASELECT_PABOOST or RF_PACONFIG_PASELECT_RFO
        - RF_PACONFIG_PASELECT_PABOOST -- LoRa single output via PABOOST, maximum output 20dBm
        - RF_PACONFIG_PASELECT_RFO     -- LoRa single output via RFO_HF / RFO_LF, maximum output 14dBm
    */
    LoRa.setTxPower(20, RF_PACONFIG_PASELECT_PABOOST);
    LoRa.beginPacket();
    LoRa.print(payload);
    LoRa.endPacket();
    payload += "\n";
    cont += 1;
    appendFile(SD, "/setup01serial.txt", payload);
  }else{
    Heltec.display->clear();
    Heltec.display->drawString(0, 50, "100 iteracoes feitas");
    Heltec.display->display();
    }
  
}
