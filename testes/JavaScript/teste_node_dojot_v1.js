const SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;
const port = new SerialPort('/dev/ttyUSB1', {baudRate: 9600});
const parser = new Readline();
port.pipe(parser);
 
 
var mqtt = require('mqtt')
//var client  = mqtt.connect('mqtt://192.168.42.28')
//var client  = mqtt.connect('mqtt://192.168.0.103')
var client  = mqtt.connect('mqtt://192.168.0.116')
//var client  = mqtt.connect('mqtt://localhost:8000')
 
console.log('inicio');
client.on('connect', function () {
console.log('conectou');    
client.subscribe('/admin/331d2c/attrs');
 
parser.on('data', (recebido_arduino) => {
  console.log("==========================");    
  console.log("recebe: " + recebido_arduino);
  var leitura_dht11 = recebido_arduino.split(':');
  console.log("Temperatura: " + leitura_dht11[0]);
  console.log("Umidade: " + leitura_dht11[1]);  
  client.publish("/admin/331d2c/attrs", '{"temp":'+ leitura_dht11[0] +'}');
  client.publish("/admin/331d2c/attrs", '{"umi":'+ leitura_dht11[1] +'}');
})  
  
})
client.on('message', function (topic, message) {  
  console.log("msg recebida => " + message.toString());  
})
