/*
  This is a simple example show the Heltec.LoRa sended data in OLED.

  This code aims to be the sender in a LoRa communication, the message is
  sended in a packet with the information provided by the sensor, this
  information that the packet have is printed also in the OLED. The message is
  organized like this "value1:valu2", the first one is umidity and the second
  one is temperature. The message is send in a packet
  that is read char by char. We define a BAND that is used in america (915E6)

  The onboard OLED display is SSD1306 driver and I2C interface. In order to make the
  OLED correctly operation, you should output a high-low-high(1-0-1) signal by soft-
  ware to OLED's reset pin, the low-level signal at least 5ms.
  Circuito
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
   DHT22
 ** Digital - pin 23
   GPS
 **
 **
   MQ9
 ** Analogico - pin 0
  OLED pins to ESP32 GPIOs via this connecthin:
  OLED_SDA -- GPIO4
  OLED_SCL -- GPIO15
  OLED_RST -- GPIO16


*/
#include "heltec.h"
#include "images.h"
#include <Adafruit_Sensor.h>
//Definicoes DHT
#include "DHT.h"
#define DHTPIN  23 //pino utilizado no esp
#define DHTTYPE DHT22   // DHT 11

#include <SPI.h>

//bib SD
#include <SD.h>
File myFile;
//sensor MQ9
#define PIN_MQ9 A0
#define VRL_VALOR 5 //resistência de carga
#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet
#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao
#define ITERACOES_LEITURA 5     //numero de leituras para analise
#define GAS_LPG 0
#define GAS_CO 1
#define SMOKE 2
float LPGCurve[3]  =  {2.3, 0.30, -0.43}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log2), p2: (log1000, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float COCurve[3]  =  {2.3, 0.23, -0.46};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, 1.7), p2(log1000, 0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float CH4Curve[3] = {2.3, 0.47, -0.37}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log3), p2: (log1000, log0.7)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float Ro = 10;

DHT dht(DHTPIN, DHTTYPE);


#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

String rssi = "RSSI --";
String packSize = "--";
String packet ;

void logo()
{
  Heltec.display->clear();
  Heltec.display->drawXbm(0, 5, logo_width, logo_height, logo_bits);
  Heltec.display->display();
}

void setup()
{
  //WIFI Kit series V1 not support Vext control
  //sensor
  dht.begin();
  //display
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);

  Heltec.display->init();
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_10);
  logo();
  delay(1500);
  Heltec.display->clear();
  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");
  Heltec.display->display();
  Ro = MQCalibration(PIN_MQ9);
  Heltec.display->clear();
  Heltec.display->drawString(0, 0, "Calibracao concluida");
  Heltec.display->display();

  delay(1000);


}

void loop()
{
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_10);

  //leitura do sensor
  float umidade = dht.readHumidity();
  float temperatura = dht.readTemperature();
  float lpg = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_LPG);
  float co = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_CO);
  float ch4 = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, SMOKE);
  
  Heltec.display->drawString(0, 0, "Sending: ");
  Heltec.display->drawString(70, 0, String(umidade));
  Heltec.display->drawString(70, 0, String(':'));
  Heltec.display->drawString(0, 15, String(temperatura));
  Heltec.display->drawString(0, 25, String(lpg));
  Heltec.display->drawString(20, 25, String(co));
  Heltec.display->drawString(40, 25, String(ch4));
  Heltec.display->display();

  // send packet
  LoRa.beginPacket();

  /*
     LoRa.setTxPower(txPower,RFOUT_pin);
     txPower -- 0 ~ 20
     RFOUT_pin could be RF_PACONFIG_PASELECT_PABOOST or RF_PACONFIG_PASELECT_RFO
       - RF_PACONFIG_PASELECT_PABOOST -- LoRa single output via PABOOST, maximum output 20dBm
       - RF_PACONFIG_PASELECT_RFO     -- LoRa single output via RFO_HF / RFO_LF, maximum output 14dBm
  */
    //We changed the Tx power to 20
  LoRa.setTxPower(20, RF_PACONFIG_PASELECT_PABOOST);
  LoRa.print(temperatura);
  LoRa.print(":");
  LoRa.print(umidade);
  LoRa.print(":");
  LoRa.print(lpg);
  LoRa.print(":");
  LoRa.print(co);
  LoRa.print(":");
  LoRa.println(ch4);
  LoRa.endPacket();

  digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
