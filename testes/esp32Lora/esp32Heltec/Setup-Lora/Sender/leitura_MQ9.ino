float leitura_MQ9(int mq_pin)
{
  int i;
  float rs = 0;

  for (i = 0; i < ITERACOES_LEITURA; i++) {
    rs += calcularResistencia(analogRead(mq_pin));
    delay(50);
  }

  rs = rs / ITERACOES_LEITURA;

  return rs;
}
