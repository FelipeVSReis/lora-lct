/*
  This is a simple example show the Heltec.LoRa sended data in OLED.

  The onboard OLED display is SSD1306 driver and I2C interface. In order to make the
  OLED correctly operation, you should output a high-low-high(1-0-1) signal by soft-
  ware to OLED's reset pin, the low-level signal at least 5ms.

  This code aims to be the sender in a LoRa communication, the message is
  sended in a packet with the information provided by the sensor, this
  information that the packet have is printed also in the OLED. The message is
  organized like this "value1:valu2", the first one is umidity and the second
  one is temperature. The message is send in a packet
  that is read char by char. We define a BAND that is used in america (915E6)

  OLED pins to ESP32 GPIOs via this connecthin:
  OLED_SDA -- GPIO4
  OLED_SCL -- GPIO15
  OLED_RST -- GPIO16
  
  by Aaron.Lee from HelTec AutoMation, ChengDu, China
  成都惠利特自动化科技有限公司
  www.heltec.cn
  
  this project also realess in GitHub:
  https://github.com/Heltec-Aaron-Lee/WiFi_Kit_series
*/
#include "heltec.h"
#include "images.h"
#include <Adafruit_Sensor.h>
#include "DHT.h"

#define DHTPIN  23 //pino utilizado no esp
#define DHTTYPE DHT22   // DHT 22

DHT dht(DHTPIN, DHTTYPE);


#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6

String rssi = "RSSI --";
String packSize = "--";
String packet ;

void logo()
{
  Heltec.display->clear();
  Heltec.display->drawXbm(0,5,logo_width,logo_height,logo_bits);
  Heltec.display->display();
}

void setup()
{
   //WIFI Kit series V1 not support Vext control
   //sensor
   dht.begin();
   //display
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
 
  Heltec.display->init();
  Heltec.display->flipScreenVertically();  
  Heltec.display->setFont(ArialMT_Plain_10);
  logo();
  delay(1500);
  Heltec.display->clear();
  
  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");
  Heltec.display->display();

  
  delay(1000);
}

void loop()
{
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_10);

  //leitura do sensor
   float umidade = dht.readHumidity();    
   float temperatura = dht.readTemperature();   
   
  Heltec.display->drawString(0, 0, "Sending DHT22: ");
  
  Heltec.display->drawString(70, 0, String(umidade));
  Heltec.display->drawString(70, 0, String(':')); 
  Heltec.display->drawString(0, 20, String(temperatura));
  Heltec.display->display();

  // send packet
  LoRa.beginPacket();
  
/*
 * LoRa.setTxPower(txPower,RFOUT_pin);
 * txPower -- 0 ~ 20
 * RFOUT_pin could be RF_PACONFIG_PASELECT_PABOOST or RF_PACONFIG_PASELECT_RFO
 *   - RF_PACONFIG_PASELECT_PABOOST -- LoRa single output via PABOOST, maximum output 20dBm
 *   - RF_PACONFIG_PASELECT_RFO     -- LoRa single output via RFO_HF / RFO_LF, maximum output 14dBm
*/
//We changed the Tx power to 20

  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.println("DH22 ");
  LoRa.print("Umidade: ");
  LoRa.print(umidade);
  LoRa.println("Temperatura: ");
  LoRa.print(temperatura); 
  LoRa.endPacket();

  digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
