/*
  Testando a comunicacao, salvamento e envio de informacoes de um arduino.
  Nesse teste foram ultizados um Data logging board, gps module, dht11 e modulo gps
  Circuito
   SD card attached to SPI bus as follows:
 **SD_CS   -- GPIO22
 **SD_MOSI -- GPIO23
 **SD_SCK  -- GPIO17
 **SD_MISO -- GPIO13
   DHT11
 ** Digital - pin 10
   GPS
 **
 **
   MQ9
 ** Digital - pin 8
 ** Analogico - pin 0

  created   Nov 2019
  by Felipe Reis
*/

#include <SPI.h>
#include <SD.h>
#include "DHT.h"
#include "FS.h"
#include <time.h>
#include <WiFi.h>

SPIClass spi1;

#define DHTPIN 2
#define DHTTYPE DHT22
#define PIN_MQ9 A0 //define pin sensor MQ-9

#define VRL_VALOR 5 //resistência de carga
#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet
#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao
#define ITERACOES_LEITURA 5     //numero de leituras para analise
#define GAS_LPG 0
#define GAS_CO 1
#define SMOKE 3
float LPGCurve[3]  =  {2.3, 0.30, -0.43}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log2), p2: (log1000, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float COCurve[3]  =  {2.3, 0.23, -0.46};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, 1.7), p2(log1000, 0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float CH4Curve[3] = {2.3, 0.47, -0.37}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log3), p2: (log1000, log0.7)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float Ro = 10;

const char* ssid     = "LCT";
const char* password = "propagacao";

long timezone = 1;
byte daysavetime = 1;

DHT dht(DHTPIN, DHTTYPE);


void setup() {
  Serial.begin(115200);
  dht.begin();
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);

  Serial.print("Calibrando o sensor MQ9...\n");
  Ro = MQCalibration(PIN_MQ9);      //calibra o sensor MQ2
  Serial.print("Calibracao concluida...\n");

  Serial.print("Initializing SD card...");

  if (!SD.begin(22, spi1)) {
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
    Serial.println("No SD card attached");
    return;
  }

  Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
    Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }
  Serial.println("initialization done.");
  Serial.print("Valor de Ro=");
  Serial.print(Ro);
  Serial.print("kohm");
  Serial.print("\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1500);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float lpg = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_LPG);
  float co = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_CO);
  float ch4 = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, SMOKE);
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  float hic = dht.computeHeatIndex(t, h, false);
  Serial.print("Writing to setup.txt...");

  String umiS;
  String tempS;
  String lpgS;
  String coS;
  String ch4S;
  umiS = String(h);
  tempS = String(t);
  lpgS = String(lpg);
  coS = String(co);
  ch4S = String (ch4);
  String payload = "{\"temp\":\"" + tempS + "\"} ";
  String payload2 = "{\"umi\":\"" + umiS + "\"} ";
  String payload3 = "{\"lpg\":\"" + lpgS + "\"} ";
  String payload4 = "{\"co\":\"" + coS + "\"} ";
  String payload5 = "{\"ch4\":\"" + ch4S + "\"}\n";
  appendFile(SD, "/setup.txt", payload);
  appendFile(SD, "/setup.txt", payload2);
  appendFile(SD, "/setup.txt", payload3);
  appendFile(SD, "/setup.txt", payload4);
  appendFile(SD, "/setup.txt", payload5);
}
