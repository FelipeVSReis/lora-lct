/*
  Enviando informacoes para o dojo por meio de wifi com backup.
  Nesse teste foram ultizados um Data logging board, gps module, dht11 e mq09
  Circuito
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
   DHT22
 ** Digital - pin 23
   GPS
 **
 **
   MQ9
 ** Analogico - pin 0

  created   Nov 2019
  by LCTTeam
*/

#include <WiFi.h>
#include <PubSubClient.h>
const char* ssid = "LCT";
const char* password =  "propagacao";
const char* mqttServer = "192.168.0.116";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "admin";

#include <SPI.h>
//bib SD
#include <SD.h>
File myFile;

//sensor MQ9
#define PIN_MQ9 A0
#define VRL_VALOR 5 //resistência de carga
#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet
#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao
#define ITERACOES_LEITURA 5     //numero de leituras para analise
#define GAS_LPG 0
#define GAS_CO 1
#define SMOKE 2
float LPGCurve[3]  =  {2.3, 0.30, -0.43}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log2), p2: (log1000, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float COCurve[3]  =  {2.3, 0.23, -0.46};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, 1.7), p2(log1000, 0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float CH4Curve[3] = {2.3, 0.47, -0.37}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log3), p2: (log1000, log0.7)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float Ro = 10;

//sensor DHT22
#include <Adafruit_Sensor.h>
#include "DHT.h"
#define DHTPIN 23  //pino utilizado no Arduino
#define DHTTYPE DHT22   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

WiFiClient espClient;

PubSubClient client(mqttServer, mqttPort, NULL, espClient);

char mqttTopic[] = "/admin/331d2c/attrs";

//conectando com o wifi
void setup()
{
  dht.begin();
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.println("Iniciando conexao com a rede WiFi..");
  }
  Serial.println("Conectado na rede WiFi!");
  Serial.println(WiFi.localIP());
  Serial.print("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  Serial.print("Calibrando o sensor MQ9...\n");
  Ro = MQCalibration(PIN_MQ9);      //calibra o sensor MQ2
  Serial.print("Calibracao concluida...\n");
  Serial.print("Valor de Ro=");
  Serial.print(Ro);
  Serial.print("kohm");
  Serial.print("\n");
}

void loop()
{
  //Faz a conexao com o broker MQTT
  reconectabroker();
  float umidade = dht.readHumidity();
  float temperatura = dht.readTemperature();
  float lpg = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_LPG);
  float co = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_CO);
  float ch4 = getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, SMOKE);
  // checa se houve falha e sai, tenta novamente
  if (isnan(umidade) || isnan(temperatura))
  {
    Serial.println("Falha na leitura do sensor!");
    return;
  }
  myFile = SD.open("Setup01.txt", FILE_WRITE);

  if (myFile) {
    Serial.print("Writing to Setup01.txt...");
    myFile.print(F("Humid: "));
    myFile.print(umidade);
    myFile.print(F("%  Temp: "));
    myFile.print(temperatura);
    myFile.print(F("°C "));
    myFile.print("LPG:");
    myFile.print(lpg );
    myFile.print( "ppm     " );
    myFile.print("CO:");
    myFile.print(co );
    myFile.print( "ppm    " );
    myFile.print("CH4:");
    myFile.print(ch4 );
    myFile.println( "ppm    " );
    // close the file:
    myFile.close();
    Serial.println("done.");
  }  else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  
  String umiS;
  String tempS;
  String lpgS;
  String coS;
  String ch4S;
  umiS = String(umidade);
  tempS = String(temperatura );
  lpgS = String(lpg);
  coS = String(co);
  ch4S = String (ch4);
  int length = 0;
  String payload = "{\"temp\":\"" + tempS + "\"}";
  int length1 = 0;
  String payload2 = "{\"umi\":\"" + umiS + "\"}";
  String payload3 = "{\"lpg\":\"" + lpgS + "\"}";
  String payload4 = "{\"co\":\"" + coS + "\"}";
  String payload5 = "{\"ch4\":\"" + ch4S + "\"}";
  length = payload.length();
  length1 = payload2.length();
  //Serial.print(F("\nData length"));
  //Serial.println(length);


  Serial.print("Sending payload: ");
  Serial.println(payload);
  Serial.print("Sending payload2: ");
  Serial.println(payload2);
  Serial.print("Sending payload3: ");
  Serial.println(payload3);
  Serial.print("Sending payload4: ");
  Serial.println(payload4);
  Serial.print("Sending payload5: ");
  Serial.println(payload5);
  


  if (client.publish(mqttTopic, (char*) payload.c_str())) {
    Serial.println("Publish payload 1 ok");

  } else {
    Serial.println("Publish failed");
    Serial.println(client.state());
  }

  if (client.publish(mqttTopic, (char*) payload2.c_str())) {
    Serial.println("Publish payload 2 ok");

  } else {
    Serial.println("Publish failed");
    Serial.println(client.state());
  }
  
  if (client.publish(mqttTopic, (char*) payload3.c_str())) {
    Serial.println("Publish payload 3 ok");

  } else {
    Serial.println("Publish failed");
    Serial.println(client.state());
  }

  if (client.publish(mqttTopic, (char*) payload4.c_str())) {
    Serial.println("Publish payload 4 ok");

  } else {
    Serial.println("Publish failed");
    Serial.println(client.state());
  }

  if (client.publish(mqttTopic, (char*) payload5.c_str())) {
    Serial.println("Publish payload 5 ok");

  } else {
    Serial.println("Publish failed");
    Serial.println(client.state());
  }

  delay(1000);
}
