void reconectabroker()
  {
    //Conexao ao broker MQTT
    client.setServer(mqttServer, mqttPort);
    while (!client.connected())
    {
      Serial.println("Conectando ao broker MQTT...");
      if (client.connect("ESP32Client", mqttUser, mqttPassword ))
      {
        Serial.println("Conectado ao broker!");
      }
      else
      {
        Serial.print("Falha na conexao ao broker - Estado: ");
        Serial.print(client.state());
        delay(2000);
      }
    }
  }
