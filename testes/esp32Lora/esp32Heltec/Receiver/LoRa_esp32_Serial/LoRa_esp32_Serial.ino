/* 
  This code aims to be the receiver in a LoRa communication, the message is
  print in the serial in 9600 baud rate. The message is send in a packet
  that is read char by char. We define a BAND that is used in america (915E6)
  https://github.com/Heltec-Aaron-Lee/WiFi_Kit_series
*/

#include "heltec.h"

#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6
void setup() {
    //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.begin(9600);
}

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    
    // read packet
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }
    
    
  }
}
