/*
  This code aims to do a communication between esp32 and dojot, by using Wi-Fi
  and MQTT in the port 1883 (secureless). The info that is going to be send
  to dojot is obtained by a LoRa communication, this code is to be used by the
  receiver.
  For this purpose, in the beggining we define some info about our network,
  MQTT, band used by the LoRa. A topic is configured to be sended in the way
  that dojot request, and after the default connection to the wi-fi and mqtt
  communication is started, and also heltec is initialized.


  This code wait for a packet that is sended by other LoRa, and this packet
  is organized to be stored in two variables to be sended to dojot.
  THIS PART IS INCOMPLETE


  In the end we send a payload in Json with the information and wait
  for a "publish ok"


*/

#include <WiFi.h>
#include <PubSubClient.h>
const char* ssid = "LCT";
const char* password =  "propagacao";
const char* mqttServer = "192.168.0.116";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "admin";

//lora
#include "heltec.h"
#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6


WiFiClient espClient;

PubSubClient client(mqttServer, mqttPort, NULL, espClient);

char mqttTopic[] = "/admin/331d2c/attrs";

//conectando com o wifi
void setup()
{
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.println("Iniciando conexao com a rede WiFi..");
  }
  Serial.println("Conectado na rede WiFi!");
  Serial.println(WiFi.localIP());
}

void reconectabroker()
{
  //Conexao ao broker MQTT
  client.setServer(mqttServer, mqttPort);
  while (!client.connected())
  {
    Serial.println("Conectando ao broker MQTT...");
    if (client.connect("ESP32Client", mqttUser, mqttPassword ))
    {
      Serial.println("Conectado ao broker!");
    }
    else
    {
      Serial.print("Falha na conexao ao broker - Estado: ");
      Serial.print(client.state());
      delay(2000);
    }
  }
}


void loop()
{
  //Faz a conexao com o broker MQTT
  reconectabroker();
  int packetSize = LoRa.parsePacket();
  String umiS ;
  String tempS ;
  String teste;
  if (packetSize) {
    // read packet
    while (LoRa.available()) {
      teste += String((char)LoRa.read());
    }
    
    umiS = teste.substring(0, 5);
    tempS = teste.substring(6);
    int length = 0;
    String payload = "{\"temp\":\"" + tempS + "\"}";
    int length1 = 0;
    String payload2 = "{\"umi\":\"" + umiS + "\"}";
    length = payload.length();
    length1 = payload2.length();
    //Serial.print(F("\nData length"));
    //Serial.println(length);


    Serial.print("Sending payload: ");
    Serial.println(payload);
    Serial.print("Sending payload2: ");
    Serial.println(payload2);


    if (client.publish(mqttTopic, (char*) payload.c_str())) {
      Serial.println("Publish payload 1 ok");

    } else {
      Serial.println("Publish failed");
      Serial.println(client.state());
    }

    if (client.publish(mqttTopic, (char*) payload2.c_str())) {
      Serial.println("Publish payload 2 ok");

    } else {
      Serial.println("Publish failed");
      Serial.println(client.state());
    }

    delay(1000);
  }


}
