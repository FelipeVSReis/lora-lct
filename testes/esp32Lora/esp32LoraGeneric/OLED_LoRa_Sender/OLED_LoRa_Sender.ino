#include <SPI.h>
#include <SD.h>
#include <Adafruit_Sensor.h>
#include "DHT.h"
#include <LoRa.h>
#include <Wire.h>  
#include "SSD1306.h" 
#include "images.h"

#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISnO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND  915E6

unsigned int counter = 0;

SPIClass spi1;

#define DHTPIN 2
#define DHTTYPE DHT11

SSD1306 display(0x3c, 4, 15); //SDA - SCL
String rssi = "RSSI --";
String packSize = "--";
String packet ;

DHT dht(DHTPIN, DHTTYPE); 

void setup() {
  pinMode(16,OUTPUT);
  pinMode(2,OUTPUT);
  
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50); 
  digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high
  
  dht.begin();
  SPIClass(1);
  spi1.begin(17, 13, 23, 22);
  
  Serial.begin(115200);
  while (!Serial);
  Serial.println();
  Serial.println("LoRa Sender Test");
  
  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);
  if (!LoRa.begin(915E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  if (!SD.begin(22, spi1)) {
    Serial.println("Card Mount Failed");
    //Heltec.display->drawString(0, 55, "Card Mount Failed");
    return;
  }
  //LoRa.onReceive(cbk);
//  LoRa.receive();
  Serial.println("init ok");
  display.init();
  display.flipScreenVertically();  
  display.setFont(ArialMT_Plain_10);
   
  delay(1500);
}

void loop() {
  display.clear();
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
  String umiS = String(h);
  String tempS = String(t);
  String payload = "temp: " + tempS + " umid: " + umiS;
  
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  
  display.drawString(0, 0, "Sending packet: ");
  display.drawString(90, 0, String(counter));
  display.drawString(0, 20, "DHT t h:");
  display.drawString(90, 20, String(t));
  display.drawString(100, 20, String(h));
  Serial.println(String(counter));
  display.display();

  // send packet
  LoRa.beginPacket();
  LoRa.print("hello ");
  LoRa.print(counter);
  LoRa.print("...");
  LoRa.print("temp: ");
  LoRa.print(t);
  LoRa.print(" umid: ");
  LoRa.print(h);
  LoRa.endPacket();

  // save packet
  appendFile(SD, "/teste.txt", payload);
  
  counter++;
  digitalWrite(2, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(2, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
