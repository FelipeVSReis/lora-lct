/*
  This code aims to send the info retrived by DHT22 in a communication 
  between esp32 and dojot, by using Wi-Fi and MQTT in the port 1883 
  (secureless). For this purpose, in the beggining  we define some info about 
  our network, after that DHT is initialized, this one is used to send the 
  info retrived by the sensor to a virtual device pre-configured in dojot.
  A topic is configured to be sended in the way that dojot request,
  and after the default connection to the wi-fi and mqtt communication is 
  started. In the end we send a payload to each attrs in Json with the
  information and wait for a "publish ok"
*/

#include <WiFi.h>
#include <PubSubClient.h>
const char* ssid = "LCT";
const char* password =  "propagacao";
const char* mqttServer = "192.168.0.116";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "admin";

//testes
int cont = 0;

//sensor DHT22
#include <Adafruit_Sensor.h>
#include "DHT.h"
#define DHTPIN 2 //pino utilizado no Arduino
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);



WiFiClient espClient;

PubSubClient client(mqttServer, mqttPort, NULL, espClient);

char mqttTopic[] = "/admin/dee7f6/attrs";

//conectando com o wifi
void setup()
{ 
  dht.begin();
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.println("Iniciando conexao com a rede WiFi..");
  }
  Serial.println("Conectado na rede WiFi!");
  Serial.println(WiFi.localIP());
}

void reconectabroker()
  {
    //Conexao ao broker MQTT
    client.setServer(mqttServer, mqttPort);
    while (!client.connected())
    {
      Serial.println("Conectando ao broker MQTT...");
      if (client.connect("ESP32Client", mqttUser, mqttPassword ))
      {
        Serial.println("Conectado ao broker!");
      }
      else
      {
        Serial.print("Falha na conexao ao broker - Estado: ");
        Serial.print(client.state());
        delay(2000);
      }
    }
  }


void loop()
    {
      //Faz a conexao com o broker MQTT
      reconectabroker();
      float umidade = dht.readHumidity();    
      float temperatura = dht.readTemperature();   
       // checa se houve falha e sai, tenta novamente
     if (isnan(umidade) || isnan(temperatura))
      {
       Serial.println("Falha na leitura do sensor!");
        return;
       } 
      
      String umiS;
      String tempS;
      umiS = String(umidade);
      tempS = String(temperatura );
      int length = 0;            
      String payload = "{\"temp\":\"" + tempS + "\"}";
      int length1 = 0;
      String payload2 = "{\"umi\":\"" + umiS + "\"}";
      length = payload.length();
      length1 = payload2.length();
      //Serial.print(F("\nData length"));
      //Serial.println(length);
    
    
      Serial.print("Sending payload: ");
      Serial.println(payload);
      Serial.print("Sending payload2: ");
      Serial.println(payload2);
    
    
      if (client.publish(mqttTopic, (char*) payload.c_str())) {
        Serial.println("Publish payload 1 ok");
        
      } else {
        Serial.println("Publish failed");
        Serial.println(client.state());
      }

     if (client.publish(mqttTopic, (char*) payload2.c_str())) {
        Serial.println("Publish payload 2 ok");
        
      } else {
        Serial.println("Publish failed");
        Serial.println(client.state());
      }
      
      delay(1000);
  }     
