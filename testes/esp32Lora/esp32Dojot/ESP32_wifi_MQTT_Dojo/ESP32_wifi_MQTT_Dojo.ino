/*
  This code aims to do a communication between esp32 and dojot, by using Wi-Fi
  and MQTT in the port 1883 (secureless). For this purpose, in the beggining
  we define some info about our network, after that is initialized a counter,
  this one is used to send an info to a virtual device pre-configured in dojot.
  A topic is configured to be sended in the way that dojot request,
  and after the default connection to the wi-fi and mqtt communication is 
  started. In the end we send a payload in Json with the information and wait
  for a "publish ok"


*/

#include <WiFi.h>
#include <PubSubClient.h>
const char* ssid = "LCT";
const char* password =  "propagacao";
const char* mqttServer = "192.168.0.116";
const int mqttPort = 1883;
const char* mqttUser = "admin";
const char* mqttPassword = "admin";

//testes
int cont = 0;

WiFiClient espClient;

PubSubClient client(mqttServer, mqttPort, NULL, espClient);

char mqttTopic[] = "/admin/331d2c/attrs";

//conectando com o wifi
void setup()
{
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.println("Iniciando conexao com a rede WiFi..");
  }
  Serial.println("Conectado na rede WiFi!");
  Serial.println(WiFi.localIP());
}

void reconectabroker()
  {
    //Conexao ao broker MQTT
    client.setServer(mqttServer, mqttPort);
    while (!client.connected())
    {
      Serial.println("Conectando ao broker MQTT...");
      if (client.connect("ESP32Client", mqttUser, mqttPassword ))
      {
        Serial.println("Conectado ao broker!");
      }
      else
      {
        Serial.print("Falha na conexao ao broker - Estado: ");
        Serial.print(client.state());
        delay(2000);
      }
    }
  }


void loop()
    {
      //Faz a conexao com o broker MQTT
      reconectabroker();
      cont+=1;
      String cont2;
      cont2 = String(cont);
      int length = 0;            
      String payload = "{\"temp\":\"" + cont2 + "\"}";
      length = payload.length();
      //Serial.print(F("\nData length"));
      //Serial.println(length);
    
    
      Serial.print("Sending payload: ");
      Serial.println(payload);
    
    
      if (client.publish(mqttTopic, (char*) payload.c_str())) {
        Serial.println("Publish ok");
        
      } else {
        Serial.println("Publish failed");
        Serial.println(client.state());
      }
      delay(1000);
  }     
