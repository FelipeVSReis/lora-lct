/*
  Testando a comunicacao, salvamento e envio de informacoes de um arduino.
  Nesse teste foram ultizados um Data logging board, gps module, dht11 e modulo gps
  Circuito
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
   DHT11
 ** Digital - pin 10
   GPS
 ** RX - pin 2
 ** TX - pin 3
   MQ9
 ** Digital - pin 8
 ** Analogico - pin 0

  created   Nov 2019
  by Felipe Reis
*/

#include <SPI.h>
#include <SD.h>

#include <SoftwareSerial.h>
#include <TimeLib.h>
#include <TinyGPS++.h>

#include "DHT.h"

#define DHTPIN 10
#define DHTTYPE DHT11
#define PIN_MQ9 A0 //define pin sensor MQ-9

//Pinos utilizados para conexao do modulo GY-NEO6MV2
static const int RXPin = 2, TXPin = 3;

#define VRL_VALOR 5 //resistência de carga
#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet
#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao
#define ITERACOES_LEITURA 5     //numero de leituras para analise
#define GAS_LPG 0
#define GAS_CO 1
#define SMOKE 2
float LPGCurve[3]  =  {2.3, 0.30, -0.43}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log2), p2: (log1000, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float COCurve[3]  =  {2.3, 0.23, -0.46};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, 1.7), p2(log1000, 0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float CH4Curve[3] = {2.3, 0.47, -0.37}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log3), p2: (log1000, log0.7)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float Ro = 10;

const int UTC_offset = -3;


File myFile;
DHT dht(DHTPIN, DHTTYPE);
TinyGPSPlus gps;
SoftwareSerial Serial_GPS(RXPin, TXPin);

void setup() {
  Serial.begin(115200);
  //Baud rate Arduino
  //Baud rate Modulo GPS
  Serial_GPS.begin(9600);
  dht.begin();
  GPS_Timezone_Adjust();
  Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  Serial.println(F("Data, Hora, Latitude e Longitude"));
  Serial.println(F("Modulo GPS GY-NEO6MV2"));
  Serial.print(F("Biblioteca TinyGPS++ v. "));
  Serial.println(TinyGPSPlus::libraryVersion());
  Serial.println();
  //  Serial.print(day());
  //  Serial.print("/");
  //  Serial.print(month());
  //  Serial.print("/");
  //  Serial.println(year());
  //  Serial.print(hour());
  //  Serial.print(":");
  //  Serial.print(minute());
  //  Serial.print(":");
  //  Serial.println(second());
  Serial.print("Calibrando o sensor MQ9...\n");
  Ro = MQCalibration(PIN_MQ9);      //calibra o sensor MQ2
  Serial.print("Calibracao concluida...\n");
  Serial.print("Valor de Ro=");
  Serial.print(Ro);
  Serial.print("kohm");
  Serial.print("\n");
  while (Serial_GPS.available() > 0)
    if (gps.encode(Serial_GPS.read()))
      Serial.print("Gps ok");

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while (true);
  }
}

void GPS_Timezone_Adjust()
{
  while (Serial_GPS.available())
  {
    if (gps.encode(Serial_GPS.read()))
    {
      int Year = gps.date.year();
      byte Month = gps.date.month();
      byte Day = gps.date.day();
      byte Hour = gps.time.hour();
      byte Minute = gps.time.minute();
      byte Second = gps.time.second();

      //Ajusta data e hora a partir dos dados do GPS
      setTime(Hour, Minute, Second, Day, Month, Year);
      //Aplica offset para ajustar data e hora
      //de acordo com a timezone
      adjustTime(UTC_offset * SECS_PER_HOUR);
    }
  }
}

float MQCalibration(int mq_pin) //funcao que calibra o sensor em um ambiente limpo utilizando a resistencia do sensor em ar limpo 9.83
{
  int i;
  float valor = 0;

  for (i = 0; i < ITERACOES_CALIBRACAO; i++) { //sao adquiridas diversas amostras e calculada a media para diminuir o efeito de possiveis oscilacoes durante a calibracao
    valor += calcularResistencia(analogRead(mq_pin));
    delay(500);
  }
  valor = valor / ITERACOES_CALIBRACAO;

  valor = valor / RO_FATOR_AR_LIMPO; //o valor lido dividido pelo R0 do ar limpo resulta no R0 do ambiente

  return valor;
}

int  calculaGasPPM(float rs_ro, float *pcurve) //Rs/R0 é fornecido para calcular a concentracao em PPM do gas em questao. O calculo eh em potencia de 10 para sair da logaritmica
{
  return (pow(10, ( ((log(rs_ro) - pcurve[1]) / pcurve[2]) + pcurve[0])));
}

float calcularResistencia(int tensao)   //funcao que recebe o tensao (dado cru) e calcula a resistencia efetuada pelo sensor. O sensor e a resistência de carga forma um divisor de tensão.

{

  return (((float)VRL_VALOR * (1023 - tensao) / tensao));

}

int getQuantidadeGasMQ(float rs_ro, int gas_id)

{
  if ( gas_id == 0 ) {

    return calculaGasPPM(rs_ro, LPGCurve);

  } else if ( gas_id == 1 ) {

    return calculaGasPPM(rs_ro, COCurve);

  } else if ( gas_id == 2 ) {

    return calculaGasPPM(rs_ro, CH4Curve);

  }
  return 0;
}

float leitura_MQ9(int mq_pin)
{
  int i;
  float rs = 0;

  for (i = 0; i < ITERACOES_LEITURA; i++) {
    rs += calcularResistencia(analogRead(mq_pin));
    delay(50);
  }

  rs = rs / ITERACOES_LEITURA;

  return rs;
}


void loop() {
  // put your main code here, to run repeatedly:
  delay(1500);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  float hic = dht.computeHeatIndex(t, h, false);

  myFile = SD.open("document.txt", FILE_WRITE);

  if (myFile) {
    Serial.print("Writing to DHT11.txt...");
    myFile.print(F("Humid: "));
    myFile.print(h);
    myFile.print(F("%  Temp: "));
    myFile.print(t);
    myFile.print(F("°C "));
    myFile.print(F("  Heat index: "));
    myFile.print(hic);
    myFile.println(F("°C "));
    myFile.print("LPG:");
    myFile.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_LPG) );
    myFile.print( "ppm     " );
    myFile.print("CO:");
    myFile.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_CO) );
    myFile.print( "ppm    " );
    myFile.print("CH4:");
    myFile.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, SMOKE) );
    myFile.println( "ppm    " );
    myFile.print(F("Location: "));
    if (gps.location.isValid())
    {
      myFile.print(gps.location.lat(), 6); //latitude
      myFile.print(F(","));
      myFile.print(gps.location.lng(), 6); //longitude
    }
    else
    {
      myFile.print(F("INVALID"));
    }
    myFile.print(F("  Date/Time: "));
    if (gps.date.isValid())
    {
      myFile.print(gps.date.day()); //dia
      myFile.print(F("/"));
      myFile.print(gps.date.month()); //mes
      myFile.print(F("/"));
      myFile.print(gps.date.year()); //ano
    }
    else
    {
      myFile.print(F("INVALID"));
    }

    myFile.print(F(" "));
    if (gps.time.isValid())
    {
      if (gps.time.hour() < 10) myFile.print(F("0"));
      myFile.print(gps.time.hour()); //hora
      myFile.print(F(":"));
      if (gps.time.minute() < 10) myFile.print(F("0"));
      myFile.print(gps.time.minute()); //minuto
      myFile.print(F(":"));
      if (gps.time.second() < 10) myFile.print(F("0"));
      myFile.print(gps.time.second()); //segundo
      myFile.print(F("."));
      if (gps.time.centisecond() < 10) myFile.print(F("0"));
      myFile.print(gps.time.centisecond());
    }
    else
    {
      myFile.print(F("INVALID"));
    }
    myFile.println();
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening document.txt");
  }
}
