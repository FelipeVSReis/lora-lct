int getQuantidadeGasMQ(float rs_ro, int gas_id)

{
  if ( gas_id == 0 ) {

    return calculaGasPPM(rs_ro, LPGCurve);

  } else if ( gas_id == 1 ) {

    return calculaGasPPM(rs_ro, COCurve);

  } else if ( gas_id == 2 ) {

    return calculaGasPPM(rs_ro, CH4Curve);

  }
  return 0;
}
