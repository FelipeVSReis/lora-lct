/*
  Testando a comunicacao, salvamento e envio de informacoes de um arduino.
  Nesse teste foram ultizados um Data logging board, gps module, dht11 e modulo gps
  Circuito
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
   DHT11
 ** Digital - pin 10
   GPS
 **
 **
   MQ9
 ** Digital - pin 8
 ** Analogico - pin 0

  created   Nov 2019
  by Felipe Reis
*/

#include <SPI.h>
#include <SD.h>
#include "DHT.h"

#define DHTPIN 10
#define DHTTYPE DHT11
#define PIN_MQ9 A0 //define pin sensor MQ-9

#define VRL_VALOR 5 //resistência de carga
#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet
#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao
#define ITERACOES_LEITURA 5     //numero de leituras para analise
#define GAS_LPG 0
#define GAS_CO 1
#define SMOKE 2
float LPGCurve[3]  =  {2.3, 0.30, -0.43}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log2), p2: (log1000, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float COCurve[3]  =  {2.3, 0.23, -0.46};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, 1.7), p2(log1000, 0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float CH4Curve[3] = {2.3, 0.47, -0.37}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log3), p2: (log1000, log0.7)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float Ro = 10;


File myFile;
DHT dht(DHTPIN, DHTTYPE);


void setup() {
  Serial.begin(9600);
  dht.begin();

  Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");
  Serial.print("Calibrando o sensor MQ9...\n");
  Ro = MQCalibration(PIN_MQ9);      //calibra o sensor MQ2
  Serial.print("Calibracao concluida...\n");
  Serial.print("Valor de Ro=");
  Serial.print(Ro);
  Serial.print("kohm");
  Serial.print("\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1500);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  float hic = dht.computeHeatIndex(t, h, false);

  myFile = SD.open("DHT11.txt", FILE_WRITE);
  if (myFile) {
    Serial.print("Writing to DHT11.txt...");
    myFile.print(F("Humid: "));
    myFile.print(h);
    myFile.print(F("%  Temp: "));
    myFile.print(t);
    myFile.print(F("°C "));
    myFile.print(F("  Heat index: "));
    myFile.print(hic);
    myFile.println(F("°C "));
    myFile.print("LPG:");
    myFile.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_LPG) );
    myFile.print( "ppm     " );
    myFile.print("CO:");
    myFile.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_CO) );
    myFile.print( "ppm    " );
    myFile.print("CH4:");
    myFile.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, SMOKE) );
    myFile.println( "ppm    " );
    // close the file:
    myFile.close();
    Serial.println("done.");
  }  else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}
