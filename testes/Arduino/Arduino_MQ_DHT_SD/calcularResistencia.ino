float calcularResistencia(int tensao)   //funcao que recebe o tensao (dado cru) e calcula a resistencia efetuada pelo sensor. O sensor e a resistência de carga forma um divisor de tensão.

{

  return (((float)VRL_VALOR * (1023 - tensao) / tensao));

}
