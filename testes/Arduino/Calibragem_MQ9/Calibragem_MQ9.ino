#define PIN_MQ9 A0 //define pin sensor MQ-9
#define VRL_VALOR 5 //resistência de carga

#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet



#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao

#define ITERACOES_LEITURA 5     //numero de leituras para analise



#define GAS_LPG 0

#define GAS_CO 1

#define SMOKE 2


float LPGCurve[3]  =  {2.3, 0.30, -0.43}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log2), p2: (log1000, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}

float COCurve[3]  =  {2.3, 0.23, -0.46};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, 1.7), p2(log1000, 0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}


float CH4Curve[3] = {2.3, 0.47, -0.37}; //curva LPG aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log200, log3), p2: (log1000, log0.7)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}

float Ro = 10;

void setup() {

  Serial.begin(9600);

  Serial.print("Calibrando o sensor MQ9...\n");

  Ro = MQCalibration(PIN_MQ9);      //calibra o sensor MQ2

  Serial.print("Finalizando a calibracao...\n");

  Serial.print("Calibracao concluida...\n");

  Serial.print("Valor de Ro=");

  Serial.print(Ro);

  Serial.print("kohm");

  Serial.print("\n");

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("LPG:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_LPG) );

  Serial.print( "ppm     " );

  Serial.print("CO:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, GAS_CO) );

  Serial.print( "ppm    " );

  Serial.print("SMOKE:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ9(PIN_MQ9) / Ro, SMOKE) );

  Serial.print( "ppm    " );

  Serial.println("");

  delay(200);
}
