#define PIN_MQ135 A1 //define pin sensor MQ-9
#define VRL_VALOR 5 //resistência de carga

#define RO_FATOR_AR_LIMPO 9.83 //resistência do sensor em ar limpo 9.83 de acordo com o datasheet
#define ITERACOES_CALIBRACAO 100    //numero de leituras para calibracao
#define ITERACOES_LEITURA 5     //numero de leituras para analise

#define GAS_CO2 0
#define GAS_CO 1
#define ALCOHOL 2
#define NH4 3
#define TOLUENO 4
#define ACETONA 5

float co2_Curve[3]  =  {1,0.38,-0.37}; //curva CO2 aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log10, log2.4), p2: (log200, log0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float co_Curve[3]  =  {1,0.45,-0.22};  //curva CO aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log10, 2.8), p2(log200, 1.45)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float alcohol_Curve[3] = {1,0.21,-0.32};//curva alcool aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log10, log1.9), p2: (log200, log0.73)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float nh4_Curve[3] = {1,0.41,-0.41}; //curva nh4 aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log10, log2.6), p2: (log100, log1)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float tolueno_Curve[3] = {1,0.2,-0.3};//curva tolueno aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log10, log1.6), p2: (log100, log0.8)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float acetona_Curve[3] = {1,0.18,-0.30};//curva acetona aproximada baseada na sensibilidade descrita no datasheet {x,y,deslocamento} baseada em dois pontos
//p1: (log10, log1.5), p2: (log200, log0.6)
//inclinacao = (Y2-Y1)/(X2-X1)
//vetor={x, y, inclinacao}
float Ro = 10;

void setup() {

  Serial.begin(9600);

  Serial.print("Calibrando o sensor MQ135...\n");

  Ro = MQCalibration(PIN_MQ135);      //calibra o sensor MQ2

  Serial.print("Finalizando a calibracao...\n");

  Serial.print("Calibracao concluida...\n");

  Serial.print("Valor de Ro=");

  Serial.print(Ro);

  Serial.print("kohm");

  Serial.print("\n");

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("CO2:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ135(PIN_MQ135) / Ro, GAS_CO2) );

  Serial.print( "ppm     " );

  Serial.print("CO:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ135(PIN_MQ135) / Ro, GAS_CO) );

  Serial.print( "ppm    " );

  Serial.print("ALCOHOL:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ135(PIN_MQ135) / Ro, ALCOHOL) );

  Serial.print( "ppm    " );

  Serial.print("NH4:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ135(PIN_MQ135) / Ro, NH4) );

  Serial.print( "ppm    " );
  
  Serial.print("TOLUENO:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ135(PIN_MQ135) / Ro, TOLUENO) );

  Serial.print( "ppm    " );

  Serial.print("ACETONa:");

  Serial.print(getQuantidadeGasMQ(leitura_MQ135(PIN_MQ135) / Ro, ACETONA) );

  Serial.print( "ppm" );

  Serial.println("");

  delay(200);
}
