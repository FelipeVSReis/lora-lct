int getQuantidadeGasMQ(float rs_ro, int gas_id)

{
  if ( gas_id == 0 ) {

    return calculaGasPPM(rs_ro, co2_Curve);

  } else if ( gas_id == 1 ) {

    return calculaGasPPM(rs_ro, co_Curve);

  } else if ( gas_id == 2 ) {

    return calculaGasPPM(rs_ro, alcohol_Curve);

  } else if ( gas_id == 3 ) {

    return calculaGasPPM(rs_ro, nh4_Curve);

  } else if ( gas_id == 4 ) {

    return calculaGasPPM(rs_ro, tolueno_Curve);

  }else if ( gas_id == 5 ) {

    return calculaGasPPM(rs_ro, acetona_Curve);

  } 
  return 0;
}
